## REDEGAL TEST - Java Developer
---
### This test attempts to solve the following use case:
&nbsp;<br>
> *En la base de datos de comercio electrónico de la compañía disponemos de la tabla PRICES que refleja el precio final (pvp) y la tarifa que aplica a un producto de una cadena entre unas fechas determinadas. A continuación se muestra un ejemplo de la tabla con los campos relevantes:*

&nbsp;<br>
 
| BRAND_ID | START_DATE | END_DATE | PRICE_LIST | PRODUCT_ID | PRIORITY | PRICE | CURRENCY
| ----------- | ----------- | ----------- |----------- | ----------- | ----------- | ----------- | ----------- 
| 1 | 2020-06-14-00.00.00 | 2020-12-31-23.59.59 | 1 | 35455 | 0 | 35.50 | EUR
| 1 | 2020-06-14-15.00.00 | 2020-06-14-18.30.00 | 2 | 35455 | 1 | 25.45 | EUR
| 1 | 2020-06-15-00.00.00 | 2020-06-15-11.00.00 | 3 | 35455 | 1 | 30.50 | EUR
| 1 | 2020-06-15-16.00.00 | 2020-12-31-23.59.59 | 4 | 35455 | 1 | 38.95 | EUR

&nbsp;<br>

## The goal:

> Construir una aplicación/servicio en SpringBoot que provea un endpoint rest de consulta  tal que: 
* *Acepte como parámetros de entrada: fecha de aplicación, identificador de producto, identificador de cadena. Devuelva como datos de salida: identificador de producto, identificador de cadena, tarifa a aplicar, fechas de aplicación y precio final a aplicar.*
* *Se debe utilizar una base de datos en memoria (tipo h2) e inicializar con los datos del ejemplo.*
* *Desarrollar unos test al endpoint rest que  validen las siguientes peticiones al servicio con los datos del ejemplo:*                                                                      
- * Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA)
- * Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA)
- * Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA)
-  * Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA)
- * Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA)

---

## How can you run the application
1. Clone repository `https://gitlab.com/sanreinoso/redegaltest.git`
2. Open your favorite IDE and open the java project
3. Run the main class **TechTestApplication.java**
4. Verify that it runs correctly, in your browser go to : `http://localhosy:8080/api/sanityTest` 
5. You should get an answer as follows *"Controller is OK"* with http status 200 OK.

&nbsp;<br>
### Run application into a docker container
1. Inside the project you will find the Dockerfile file, with this file you can create a docker image that will be stored on your local computer. Build de image using the following code: `docker build . -t <image-name>`
2. Executes the container exposing the port where the application is running: `docker run -p 8080:8080 <image-name>`