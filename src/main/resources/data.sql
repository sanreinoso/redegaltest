INSERT INTO brand (brand_id, brand_name, description) VALUES (1, 'ZARA', 'Empresa española');

INSERT INTO product (product_id, name, brand_brand_id) VALUES (35455, 'Sudadera', 1);

INSERT INTO price (price_list, product_id, brand_id, price, start_date, end_date, priority, currency)
    VALUES (1, 35455, 1, 35.50, '2020-06-14 00:00:00', '2020-12-31 23:59:59', 0, 'EUR');

INSERT INTO price (price_list, product_id, brand_id, price, start_date, end_date, priority, currency)
    VALUES (2, 35455, 1, 25.45, '2020-06-14 15:00:00', '2020-06-14 18:30:00', 1, 'EUR');

INSERT INTO price (price_list, product_id, brand_id, price, start_date, end_date, priority, currency)
    VALUES (3, 35455, 1, 30.50, '2020-06-15 00:00:00', '2020-06-15 11:00:00', 1, 'EUR');

INSERT INTO price (price_list, product_id, brand_id, price, start_date, end_date, priority, currency)
    VALUES (4, 35455, 1, 30.50, '2020-06-15 16:00:00', '2020-12-31 23:59:59', 1, 'EUR');
