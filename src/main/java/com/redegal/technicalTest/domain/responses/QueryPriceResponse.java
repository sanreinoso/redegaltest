package com.redegal.technicalTest.domain.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryPriceResponse {

    private long product_id;
    private long brand_id;
    private long price_list_id;
    private LocalDateTime applicationDate;
    private double final_price;
    private String message;

    public QueryPriceResponse(String msg) {
        this.message = msg;
    }

}
