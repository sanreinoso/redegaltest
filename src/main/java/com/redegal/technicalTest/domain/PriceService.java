package com.redegal.technicalTest.domain;

import com.redegal.technicalTest.domain.entities.Price;
import com.redegal.technicalTest.infrastrucutre.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PriceService {

    @Autowired
    private PriceRepository priceRepository;

    public List<Price> pricesByProductAndBrand(long product, long brand ) {

        return this.priceRepository.findByProductAndBrand(product, brand);
    }

    public Price calculatePrice(LocalDateTime date, long product, long brand ) {

        List<Price> productPrices = this.pricesByProductAndBrand(product,brand);
        if(productPrices.isEmpty()) {
            throw new NullPointerException("Not prices found for product id: " + product);
        }
        List<Price> filteredList = productPrices.stream()
                .filter(price -> price.isInTimeRange(date)).collect(Collectors.toList());

        Price finalPrice = null;
        int priority = 0;
        for (Price price: filteredList) {
            if(price.getPriority() >= priority ) {
                finalPrice = price;
                priority = price.getPriority();
            }
        }

        return finalPrice;
    }


}
