package com.redegal.technicalTest;

import com.redegal.technicalTest.domain.entities.Price;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechTestApplication.class, args);
	}


}
