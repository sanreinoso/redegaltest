package com.redegal.technicalTest.application;

import com.redegal.technicalTest.domain.PriceService;
import com.redegal.technicalTest.domain.entities.Price;
import com.redegal.technicalTest.domain.responses.QueryPriceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class PriceController {

    @Autowired
    private PriceService priceService;

    @GetMapping("/sanityTest")
    public ResponseEntity<String> test() {
        return new ResponseEntity<String>( "Controller is OK", HttpStatus.ACCEPTED);
    }

    @GetMapping("/queryPrices")
    public ResponseEntity<QueryPriceResponse> queryPrice(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") LocalDateTime date,
                                                         @RequestParam long product_id,
                                                         @RequestParam long brand_id) {

        Price price = this.priceService.calculatePrice(date, product_id, brand_id);
        if(price == null) {
            return new ResponseEntity<QueryPriceResponse>( new QueryPriceResponse("Price not found for this date"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<QueryPriceResponse>(
                new QueryPriceResponse(price.getProduct().getProduct_id(),
                        price.getBrand().getBrand_id(),
                        price.getPrice_list(), date,
                        price.getPrice(), "Successfully applied rate"), HttpStatus.OK );
    }
}
