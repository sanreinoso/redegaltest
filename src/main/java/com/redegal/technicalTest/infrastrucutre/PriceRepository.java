package com.redegal.technicalTest.infrastrucutre;

import com.redegal.technicalTest.domain.entities.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {

    @Query("SELECT p FROM Price p WHERE p.product.product_id = :productId AND p.brand.brand_id = :brandId")
    List<Price> findByProductAndBrand(long productId, long brandId);
}
