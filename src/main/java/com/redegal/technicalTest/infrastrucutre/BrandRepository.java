package com.redegal.technicalTest.infrastrucutre;

import com.redegal.technicalTest.domain.entities.Brand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandRepository extends JpaRepository<Brand, Long> {
}
