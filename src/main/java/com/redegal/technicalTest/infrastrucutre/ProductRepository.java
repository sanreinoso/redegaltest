package com.redegal.technicalTest.infrastrucutre;

import com.redegal.technicalTest.domain.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
