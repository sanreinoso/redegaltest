package com.redegal.technicalTest;

import com.redegal.technicalTest.domain.PriceService;
import com.redegal.technicalTest.domain.entities.Price;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


@SpringBootTest
class TechTestApplicationTests {

	@Autowired
	PriceService priceService;

	@Test
	void Test1() {
		long expectedResult = 1;
		Price price = this.priceService.calculatePrice(
				LocalDateTime.of(2020, 6, 14, 10, 0),
				35455, 1);

		assertEquals(expectedResult, price.getPrice_list());
	}

	@Test
	void Test2() {
		long expectedResult = 2;
		Price price = this.priceService.calculatePrice(
				LocalDateTime.of(2020, 6, 14, 16, 0),
				35455, 1);

		assertEquals(expectedResult, price.getPrice_list());
	}

	@Test
	void Test3() {
		long expectedResult = 1;
		Price price = this.priceService.calculatePrice(
				LocalDateTime.of(2020, 6, 14, 21, 0),
				35455, 1);

		assertEquals(expectedResult, price.getPrice_list());
	}

	@Test
	void Test4() {
		long expectedResult = 3;
		Price price = this.priceService.calculatePrice(
				LocalDateTime.of(2020, 6, 15, 10, 0),
				35455, 1);

		assertEquals(expectedResult, price.getPrice_list());
	}

	@Test
	void Test5() {
		long expectedResult = 4;
		Price price = this.priceService.calculatePrice(
				LocalDateTime.of(2020, 6, 15, 21, 0),
				35455, 1);

		assertEquals(expectedResult, price.getPrice_list());
		}

}
