package com.redegal.technicalTest;

import com.redegal.technicalTest.application.PriceController;
import com.redegal.technicalTest.domain.PriceService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class WebMockTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private PriceService service;

    @Test
    void TestController() throws Exception {

        MvcResult result = mockMvc.perform(get("/sanityTest"))
                .andExpect(status().isAccepted())
                .andReturn();

        assertEquals("Controller is OK", result.getResponse().getContentAsString());
    }
}
