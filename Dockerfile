FROM openjdk:17-alpine

RUN apk add --no-cache maven

WORKDIR /app

COPY pom.xml .

RUN mvn dependency:go-offline -B

COPY . .

RUN mvn package

CMD ["java", "-jar", "target/technicalTest-0.0.1-SNAPSHOT.jar"]
